package xyz.smouquet.ducktales.test.integration.data;

import org.junit.runner.RunWith;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Base classe for Elasticsearch integration test.
 * 
 * @author smouquet
 */
@RunWith(SpringJUnit4ClassRunner.class)
@IntegrationTest({
    "spring.data.elasticsearch.properties.node.local=true",
    "spring.data.elasticsearch.properties.node.data=true",
    "spring.data.elasticsearch.properties.http.enabled=false",
    "spring.data.elasticsearch.properties.path.home=build/test-integration"
})
public class ElasticsearchIntegrationTest
{
    
}
