package xyz.smouquet.ducktales.test.integration.data;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.data.domain.Sort;

import xyz.smouquet.ducktales.app.module.base.config.ModuleBaseIntegrationTestSpringConfig;
import xyz.smouquet.ducktales.app.module.base.data.entity.Wallet;
import xyz.smouquet.ducktales.app.module.base.data.repository.WalletRepository;


@SpringApplicationConfiguration(classes=ModuleBaseIntegrationTestSpringConfig.class)       
public class SampleRepositoryIntegrationTest extends ElasticsearchIntegrationTest
{
    @Autowired
    private WalletRepository repository;

    
    @Test
    public void shouldReturnAllWalletsOrdered()
    {
        Iterable<Wallet> result = repository.findAll(new Sort("order"));
        
        assertThat(result).isNotNull();
        assertThat(result).isNotEmpty();
        assertThat(result).extracting("name", "order")
                          .containsExactly(tuple("Family accounts", 1),
                                           tuple("Employee savings", 2),
                                           tuple("My accounts", 3));
    }
    
}
