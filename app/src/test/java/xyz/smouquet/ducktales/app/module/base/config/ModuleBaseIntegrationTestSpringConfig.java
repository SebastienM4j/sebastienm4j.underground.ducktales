package xyz.smouquet.ducktales.app.module.base.config;

import java.io.File;

import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.init.Jackson2RepositoryPopulatorFactoryBean;
import org.springframework.util.FileSystemUtils;

/**
 * Base module integration test configuration.
 * 
 * @author smouquet
 */
@Configuration
@Import(ModuleBaseTestSpringConfig.class)
public class ModuleBaseIntegrationTestSpringConfig
{   

    @Bean
    public Jackson2RepositoryPopulatorFactoryBean repositoryPopulator()
    {   
        Jackson2RepositoryPopulatorFactoryBean factory = new Jackson2RepositoryPopulatorFactoryBean();
        factory.setResources(new Resource[]{
            new ClassPathResource("dataset-wallet.json") 
        });
        
        return factory;
    }
    
    
    @PreDestroy
    public void cleanUp() throws Exception
    {
        FileSystemUtils.deleteRecursively(new File("./build/test-integration/data"));
    }
    
}
