package xyz.smouquet.ducktales.app.module.base.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

/**
 * Base module test configuration.
 * 
 * @author smouquet
 */
@SpringBootApplication
@Import(ModuleBaseSpringConfig.class)
public class ModuleBaseTestSpringConfig
{   
    
}
