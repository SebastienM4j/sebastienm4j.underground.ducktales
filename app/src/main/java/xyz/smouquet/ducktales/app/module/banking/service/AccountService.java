package xyz.smouquet.ducktales.app.module.banking.service;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import reactor.bus.EventBus;
import reactor.bus.selector.Selectors;
import xyz.smouquet.ducktales.app.module.banking.data.entity.Account;
import xyz.smouquet.ducktales.app.module.banking.data.repository.AccountRepository;
import xyz.smouquet.ducktales.app.module.base.data.entity.Wallet;
import xyz.smouquet.ducktales.app.module.base.event.WalletEventKey;
import xyz.smouquet.ducktales.app.module.base.logging.Log;
import static xyz.smouquet.ducktales.app.module.base.logging.Markers.AUDIT;
import static xyz.smouquet.ducktales.app.module.banking.logging.BankingMDC.ACT;
import static xyz.smouquet.ducktales.app.module.base.logging.BaseMDC.WLT;

/**
 * Account service.
 * 
 * @author smouquet
 */
@Service
public class AccountService
{
    private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
    
    
    @Autowired
    private AccountRepository accountRepository;
    
    @Autowired
    private EventBus eventBus;

    
    @PostConstruct
    public void subscribe()
    {
        eventBus.on(Selectors.object(WalletEventKey.AFTER_DELETE), evt -> this.deleteByWallet((Wallet)evt.getData()));
    }
    
    
    /**
     * Create a new account.
     * 
     * @param account Account to create (non <code>null</code>)
     * @param walletId Id of the wallet in which create an account
     *                 (not required but <code>account#walletId</code> must be defined)
     * @return The created account
     */
    @Log(ACT)
    public Account create(Account account, @Log(WLT) String walletId)
    {
        Assert.notNull(account);
        Assert.isNull(account.getId());
        Assert.isTrue(!StringUtils.isEmpty(account.getWalletId()) ||
                      !StringUtils.isEmpty(walletId));
        
        account.setId(UUID.randomUUID().toString());
        if(StringUtils.isEmpty(account.getWalletId())) {
            account.setWalletId(walletId);
        }
        account.setCreationDate(Instant.now());
        
        Account result = accountRepository.save(account);
        
        MDC.put(ACT, account.getId());
        logger.info(AUDIT, "[ACT_CREATED] New account with id [{}] has been created", account.getId());
        
        return result;
    }
    
    /**
     * Update a account.
     * 
     * @param account Account to update (non <code>null</code>)
     */
    @Log({WLT, ACT})
    public void update(Account account)
    {
        Assert.notNull(account);
        Assert.hasText(account.getId());
        
        accountRepository.save(account);
        
        MDC.put(WLT, account.getWalletId());
        MDC.put(ACT, account.getId());
        logger.info(AUDIT, "[ACT_UPDATED] Account with id [{}] has been updated", account.getId());
    }
    
    /**
     * Delete a account by this id.
     * 
     * @param accountId Id of account to delete (non <code>null</code>)
     */
    public void deleteById(@Log(ACT) String accountId)
    {
        Assert.hasText(accountId);
        
        accountRepository.delete(accountId);
        
        logger.info(AUDIT, "[ACT_DELETED] Account with id [{}] has been deleted", accountId);
    }
    
    /**
     * Delete all accounts of the specified wallet.
     * 
     * @param wallet Wallet for which delete accounts (non <code>null</code>)
     */
    @Log({WLT, ACT})
    public void deleteByWallet(Wallet wallet)
    {
        Assert.notNull(wallet);
        Assert.hasText(wallet.getId());
        
        MDC.put(WLT, wallet.getId());
        
        List<Account> accountsToDelete = accountRepository.findByWalletIdOrderByOrderAsc(wallet.getId());
        for(Account accountToDelete : accountsToDelete)
        {
            accountRepository.delete(accountToDelete);
            
            MDC.put(ACT, accountToDelete.getId());
            logger.info(AUDIT, "[ACT_DELETED] Account with id [{}] has been deleted", accountToDelete.getId());
            MDC.remove(ACT);
            
            // TODO suppression des opérations
        }
    }
    
}
