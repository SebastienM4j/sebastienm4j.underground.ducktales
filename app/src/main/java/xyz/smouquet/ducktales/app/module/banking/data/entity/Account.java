package xyz.smouquet.ducktales.app.module.banking.data.entity;

import java.time.Instant;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

/**
 * Account entity.
 * 
 * @author smouquet
 */
@Document(indexName="#{@bankingIndexName}", type="account", shards=1, replicas=0)
public class Account
{
    @Id
    private String id;
    private String walletId;
    private String name;
    private String bankCode;
    private String officeCode;
    private String number;
    @Field(type=FieldType.Integer)
    private Integer order;
    private Instant creationDate;
    
    
    public Account() {}

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    public String getWalletId() {
        return walletId;
    }
    
    public void setWalletId(String walletId) {
        this.walletId = walletId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getBankCode() {
        return bankCode;
    }
    
    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }
    
    public String getOfficeCode() {
        return officeCode;
    }
    
    public void setOfficeCode(String officeCode) {
        this.officeCode = officeCode;
    }
    
    public String getNumber() {
        return number;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
    
    public Instant getCreationDate() {
        return creationDate;
    }
    
    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }
    
}
