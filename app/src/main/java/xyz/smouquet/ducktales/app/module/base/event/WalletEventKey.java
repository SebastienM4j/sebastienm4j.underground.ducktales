package xyz.smouquet.ducktales.app.module.base.event;

import reactor.bus.EventBus;

/**
 * Wallet event key to be used in the {@link EventBus}.
 * 
 * @author smouquet
 */
public enum WalletEventKey
{
    AFTER_DELETE
}
