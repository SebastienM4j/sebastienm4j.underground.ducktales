package xyz.smouquet.ducktales.app.module.base.logging;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotation to record values in the MDC (http://logback.qos.ch/manual/mdc.html).
 * 
 * <p>The annotation may be applied to two places :
 * <ul>
 *     <li>
 *        <b>On a method</b> : it records the current state of MCD for the given key 
 *        before entering the annotated method and puts this state in output method.
 *        We can then use {@link org.slf4j.MDC#put(String, String)} in the method body
 *        without fear of contaminating the rest of the execution.
 *    </li>
 *    <li>
 *       <b>On a field</b> : as for the method, it records the existing data 
 *       and places it at the output. In addition, it updates the MDC annotated 
 *       with the value parameter.</li>
 * </ul>
 * 
 * @author smouquet
 */
@Target({ ElementType.METHOD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface Log
{
    /** MDC keys to use */
    String[] value();
}
