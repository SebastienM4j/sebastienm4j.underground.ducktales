package xyz.smouquet.ducktales.app.module.base.logging;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.LinkedList;
import java.util.List;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;

/**
 * Aspect to manage the behavior of the annotation {@link Log}.
 */
@Aspect
@Component
public class MDCAspect
{
    
    @Around("@annotation(Log) || execution(* *(.., @Log (*), ..))")
    private Object executeMethod(ProceedingJoinPoint pjp) throws Throwable
    {
        List<OldMDC> toRemove = new LinkedList<>();

        // Annotation placed on method
        MethodSignature signature = (MethodSignature) pjp.getSignature();
        Method method = signature.getMethod();
        Log methodLog = method.getAnnotation(Log.class);

        if(methodLog != null) {
            addToMDC(toRemove, methodLog, null, false);
        }

        // Annotation placed on parameters
        Annotation[][] annotations = method.getParameterAnnotations();
        Object[] args = pjp.getArgs();
        int paramI = 0;
        for(Annotation[] annotation : annotations)
        {
            for(Annotation ann : annotation)
            {
                if(ann instanceof Log)
                {
                    Log log = (Log)ann;
                    if(log.value().length > 1) {
                        throw new IllegalArgumentException("Only one key is possible on parameters");
                    }
                    addToMDC(toRemove, log, args[paramI], true);
                }
            }
            paramI++;
        }

        try {
            return pjp.proceed();
            
        } finally {
            // After execution, MDC are removed
            for (OldMDC oldMDC : toRemove) {
                if (oldMDC.value == null) {
                    MDC.remove(oldMDC.key);
                } else {
                    MDC.put(oldMDC.key, oldMDC.value);
                }
            }
        }
    }

    /**
     * Adds value in backuping the old MDC.
     * 
     * @param toRemove List to restore the old values after treatment
     * @param log Annotation placed on the method or parameter
     * @param newValue New value to be recorded in the MDC
     * @param forceNull If <code>newValue<code> is <code>null</code>,
     *                  and <code>forceNull</code> is <code>true</true>
     *                  the current MDC value is removed otherwise (<code>false</true>)
     *                  she is kept
     */
    private void addToMDC(List<OldMDC> toRemove, Log log, Object newValue, boolean forceNull)
    {
        String[] keys = log.value();
        for(String key : keys)
        {
            String oldValue = MDC.get(key);
    
            if(oldValue == null) {
                toRemove.add(new OldMDC(key));
            } else if(!oldValue.equals(newValue)) {
                toRemove.add(new OldMDC(key, oldValue));
            }
    
            if(newValue != null && !newValue.equals(oldValue)) {
                MDC.put(key, newValue.toString());
            } else if(newValue == null && forceNull) {
                MDC.remove(key);
            }
        }
    }

    
    /**
     * Backup of MDC value.
     */
    private static final class OldMDC
    {
        public String key;
        public String value;

        public OldMDC(String key)
        {
            this.key = key;
        }

        public OldMDC(String key, String value)
        {
            this.key = key;
            this.value = value;
        }
    }
}
