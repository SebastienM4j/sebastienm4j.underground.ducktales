package xyz.smouquet.ducktales.app;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Import;

import xyz.smouquet.ducktales.app.config.CommonSpringConfig;
import xyz.smouquet.ducktales.app.config.EventBusSpringConfig;
import xyz.smouquet.ducktales.app.module.banking.config.ModuleBankingSpringConfig;
import xyz.smouquet.ducktales.app.module.base.config.ModuleBaseSpringConfig;


/**
 * Main class.
 * 
 * @author smouquet
 */
@SpringBootApplication
@Import({
    CommonSpringConfig.class,
    EventBusSpringConfig.class,
    ModuleBaseSpringConfig.class,
    ModuleBankingSpringConfig.class
})
public class DuckTalesApp extends SpringBootServletInitializer
{
    
    /**
     * Standalone application entry point.
     * 
     * @param args Command line arguments
     */
    public static void main(String[] args)
    {
        SpringApplication app = new SpringApplication(DuckTalesApp.class);
        app.setDefaultProperties(defaultProperties());
        app.run(args);
    }
    
    /*
     * Web application configuration.
     * 
     * (non-Javadoc)
     * @see org.springframework.boot.context.web.SpringBootServletInitializer#configure(org.springframework.boot.builder.SpringApplicationBuilder)
     */
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder)
    {
        builder.sources(DuckTalesApp.class)
               .application().setDefaultProperties(defaultProperties());
        
        return builder;
    }
    
    
    /**
     * @return default properties for application configuration
     */
    private static Map<String, Object> defaultProperties()
    {
        HashMap<String, Object> properties = new HashMap<>();
        
        properties.put("server.port", 8420);
        
        properties.put("spring.data.elasticsearch.properties.node.local", true);
        properties.put("spring.data.elasticsearch.properties.node.data", true);
        properties.put("spring.data.elasticsearch.properties.http.enabled", false);
        // FIXME voir quel path pour les données elasticsearch compatible à la fois java app et war
        //properties.put("spring.data.elasticsearch.properties.path.home", "../");
        
        return properties;
    }
    
    
    public DuckTalesApp() {}
    
}
