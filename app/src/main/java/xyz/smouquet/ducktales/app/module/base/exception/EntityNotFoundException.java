package xyz.smouquet.ducktales.app.module.base.exception;

/**
 * Exception throws when a searched entity is not found.
 * 
 * @author smouquet
 */
public class EntityNotFoundException extends RuntimeException
{
    private static final long serialVersionUID = -1177971917641803136L;

    
    public EntityNotFoundException() {
        super();
    }

    public EntityNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public EntityNotFoundException(String message) {
        super(message);
    }

    public EntityNotFoundException(Throwable cause) {
        super(cause);
    }
    
}
