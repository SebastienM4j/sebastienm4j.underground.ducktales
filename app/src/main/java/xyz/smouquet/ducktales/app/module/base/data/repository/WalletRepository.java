package xyz.smouquet.ducktales.app.module.base.data.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import xyz.smouquet.ducktales.app.module.base.data.entity.Wallet;

/**
 * Wallet data repository.
 * 
 * @author smouquet
 */
public interface WalletRepository extends ElasticsearchRepository<Wallet, String>
{
    
}
