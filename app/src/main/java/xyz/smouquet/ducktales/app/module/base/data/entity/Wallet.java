package xyz.smouquet.ducktales.app.module.base.data.entity;

import java.time.Instant;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Wallet entity.
 * 
 * @author smouquet
 */
@Document(indexName="#{@baseIndexName}", type="wallet", shards=1, replicas=0)
public class Wallet
{
    @Id
    private String id;
    private String name;
    private Integer order;
    private Instant creationDate;
    
    
    public Wallet() {}

    
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
    
    public Instant getCreationDate() {
        return creationDate;
    }
    
    public void setCreationDate(Instant creationDate) {
        this.creationDate = creationDate;
    }
    
}
