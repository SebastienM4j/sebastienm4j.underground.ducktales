package xyz.smouquet.ducktales.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import reactor.Environment;
import reactor.bus.EventBus;

/**
 * Event bus configuration.
 * 
 * @author smouquet
 */
@Configuration
public class EventBusSpringConfig
{
    @Bean
    public Environment eventBusEnvironment()
    {
        return Environment.initializeIfEmpty()
                          .assignErrorJournal();
    }
    
    @Bean
    public EventBus eventBus(Environment eventBusEnvironment)
    {
        return EventBus.create(eventBusEnvironment, Environment.THREAD_POOL);
    }
}
