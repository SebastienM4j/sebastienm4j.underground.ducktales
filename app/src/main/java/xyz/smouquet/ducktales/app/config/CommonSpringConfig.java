package xyz.smouquet.ducktales.app.config;

import org.elasticsearch.client.Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.EntityMapper;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.jsr310.JSR310Module;


/**
 * Spring common configuration.
 * 
 * @author smouquet
 */
@Configuration
public class CommonSpringConfig
{
    @Autowired
    private Jackson2ObjectMapperBuilder objectMapperBuilder;
    
    @Autowired
    private Client elasticsearchClient;
    
    
    /**
     * @return Jackson module for Java 8 Date/Time API support
     */
    @Bean
    public Module jsr310()
    {
        return new JSR310Module();
    }
    
    /**
     * @return Elasticsearch {@link EntityMapper} for Java 8 Date/Time API support
     */
    @Bean
    public EntityMapper jsr310EntityMapper()
    {
        return new ElasticsearchJsr310EntityMapper(objectMapperBuilder);
    }
    
    /**
     * @return {@link ElasticsearchTemplate} with Java 8 Date/Time API support
     */
    @Bean
    public ElasticsearchTemplate elasticsearchTemplate()
    {
        return new ElasticsearchTemplate(elasticsearchClient, jsr310EntityMapper());
    }
    
}
