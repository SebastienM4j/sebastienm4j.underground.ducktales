package xyz.smouquet.ducktales.app.module.banking.web.resource;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import xyz.smouquet.ducktales.app.module.banking.data.entity.Account;
import xyz.smouquet.ducktales.app.module.banking.data.repository.AccountRepository;
import xyz.smouquet.ducktales.app.module.banking.service.AccountService;
import xyz.smouquet.ducktales.app.module.base.exception.EntityNotFoundException;

/**
 * Account REST resource for GUI uses.
 * 
 * <p>This resource is dedicated to the specified
 * wallet (by <code>walletId</code> path variable).
 * So the accounts list is the accounts list of <b>one</b>
 * wallet.
 * 
 * @author smouquet
 */
@RestController
@RequestMapping("/rest/gui/wallets/{walletId}/accounts")
public class AccountResource
{
    @Autowired
    private AccountRepository accountRepository;
    
    @Autowired
    private AccountService accountService;
    
    
    /**
     * @param walletId Id of the wallet for which get the list of accounts
     * @return All accounts in ascending order
     */
    @RequestMapping(method=RequestMethod.GET)
    public List<Account> getAll(@PathVariable String walletId)
    {
        return accountRepository.findByWalletIdOrderByOrderAsc(walletId);
    }
    
    /**
     * Create a new account.
     * 
     * @param walletId Id of the wallet in which create an account
     * @param account Account to create
     * @return The created account
     * @see AccountService#create(Account, String)
     */
    @RequestMapping(method=RequestMethod.POST)
    public Account create(@PathVariable String walletId, @RequestBody Account account)
    {
        return accountService.create(account, walletId);
    }
    
    /**
     * Update a account.
     * 
     * @param walletId Id of the wallet that owns the account
     * @param accountId Id of account to update
     * @param account Account to update
     * @see AccountService#update(Account)
     */
    @RequestMapping(value="/{accountId}", method=RequestMethod.PUT)
    public void update(@PathVariable String walletId, @PathVariable String accountId, @RequestBody Account account)
    {
        Assert.notNull(account);
        Assert.isTrue(accountId.equals(account.getId()));
        Assert.isTrue(walletId.equals(account.getWalletId()));
            
        accountService.update(account);
    }
    
    /**
     * Delete a account.
     * 
     * @param walletId Id of the wallet that owns the account
     * @param accountId Id of account to delete
     * @see AccountService#deleteById(String)
     */
    @RequestMapping(value="/{accountId}", method=RequestMethod.DELETE)
    public void delete(@PathVariable String walletId, @PathVariable String accountId)
    {
        Account account = accountRepository.findOne(accountId);
        if(account == null) {
            throw new EntityNotFoundException();
        }
        Assert.isTrue(walletId.equals(account.getWalletId()));
        
        accountService.deleteById(accountId);
    }
    
}
