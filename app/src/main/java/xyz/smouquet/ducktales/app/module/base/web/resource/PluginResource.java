package xyz.smouquet.ducktales.app.module.base.web.resource;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import xyz.smouquet.ducktales.app.module.base.web.dto.PluginDTO;
import xyz.smouquet.ducktales.app.plugin.Plugin;
import xyz.smouquet.ducktales.app.plugin.PluginManagerHolder;

/**
 * Plugins REST resource for GUI uses.
 * 
 * @author smouquet
 */
@RestController
@RequestMapping("/rest/gui/plugins")
public class PluginResource
{

    /** 
     * @return All plugins
     */
    @RequestMapping(method=RequestMethod.GET)
    public List<PluginDTO> getAll()
    {
        List<Plugin> plugins = PluginManagerHolder.get().getPlugins();
        return plugins.stream()
                      .map(p -> new PluginDTO(p))
                      .collect(Collectors.toList());
    }
    
}
