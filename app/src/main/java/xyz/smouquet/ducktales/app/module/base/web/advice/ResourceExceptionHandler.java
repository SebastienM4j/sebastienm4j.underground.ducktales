package xyz.smouquet.ducktales.app.module.base.web.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import xyz.smouquet.ducktales.app.module.base.exception.EntityNotFoundException;

/**
 * {@link ExceptionHandler} commons to all resources.
 * 
 * @author smouquet
 */
@ControllerAdvice
public class ResourceExceptionHandler
{

    @ExceptionHandler(EntityNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public void handleNotFound() {}
    
    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
    public void handleUnprocessableEntity() {} 
    
}
