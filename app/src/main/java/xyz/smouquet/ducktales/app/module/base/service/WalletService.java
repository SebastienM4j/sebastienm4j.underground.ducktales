package xyz.smouquet.ducktales.app.module.base.service;

import java.time.Instant;
import java.util.UUID;

import org.apache.log4j.MDC;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import reactor.bus.Event;
import reactor.bus.EventBus;
import xyz.smouquet.ducktales.app.module.base.data.entity.Wallet;
import xyz.smouquet.ducktales.app.module.base.data.repository.WalletRepository;
import xyz.smouquet.ducktales.app.module.base.event.WalletEventKey;
import xyz.smouquet.ducktales.app.module.base.logging.Log;
import static xyz.smouquet.ducktales.app.module.base.logging.Markers.AUDIT;
import static xyz.smouquet.ducktales.app.module.base.logging.BaseMDC.WLT;

/**
 * Wallet service.
 * 
 * @author smouquet
 */
@Service
public class WalletService
{   
    private static final Logger logger = LoggerFactory.getLogger(WalletService.class);
    
    
    @Autowired
    private WalletRepository walletRepository;
    
    @Autowired
    private EventBus eventBus;

    
    /**
     * Create a new wallet.
     * 
     * @param wallet Wallet to create (non <code>null</code>)
     * @return The created wallet
     */
    @Log(WLT)
    public Wallet create(Wallet wallet)
    {
        Assert.notNull(wallet);
        Assert.isNull(wallet.getId());
        
        wallet.setId(UUID.randomUUID().toString());
        wallet.setCreationDate(Instant.now());
        Wallet result = walletRepository.save(wallet);
        
        MDC.put(WLT, wallet.getId());
        logger.info(AUDIT, "[WLT_CREATED] New wallet with id [{}] named [{}] has been created", wallet.getId(), wallet.getName());
        
        return result;
    }
    
    /**
     * Update a wallet.
     * 
     * @param wallet Wallet to update (non <code>null</code>)
     */
    @Log(WLT)
    public void update(Wallet wallet)
    {
        Assert.notNull(wallet);
        Assert.hasText(wallet.getId());
        
        walletRepository.save(wallet);
        
        MDC.put(WLT, wallet.getId());
        logger.info(AUDIT, "[WLT_UPDATED] Wallet with id [{}] named (new value) [{}] has been updated", wallet.getId(), wallet.getName());
    }
    
    /**
     * Delete a wallet.
     * 
     * @param wallet Wallet to delete (non <code>null</code>)
     */
    @Log(WLT)
    public void delete(Wallet wallet)
    {
        Assert.notNull(wallet);
        
        walletRepository.delete(wallet.getId());
        
        MDC.put(WLT, wallet.getId());
        logger.info(AUDIT, "[WLT_DELETED] Wallet with id [{}] named [{}] has been deleted", wallet.getId(), wallet.getName());
        
        eventBus.notify(WalletEventKey.AFTER_DELETE, Event.wrap(wallet));
    }
}
