package xyz.smouquet.ducktales.app.module.base.logging;

import xyz.smouquet.ducktales.app.module.base.data.entity.Wallet;

/**
 * Base module MDC (http://logback.qos.ch/manual/mdc.html) constants.
 * 
 * @author smouquet
 */
public final class BaseMDC
{
    /** MDC for {@link Wallet#getId()} */
    public static final String WLT = "wlt.id";

}
