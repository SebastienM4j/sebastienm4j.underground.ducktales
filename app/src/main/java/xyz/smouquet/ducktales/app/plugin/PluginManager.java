package xyz.smouquet.ducktales.app.plugin;

import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.stream.Collectors;

import org.apache.commons.lang.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.embedded.AnnotationConfigEmbeddedWebApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.util.Assert;

/**
 * For the plugins management. It's responsible to load
 * and register plugins in the Spring context and maintain
 * a list of plugins.
 * 
 * @author smouquet
 */
public class PluginManager
{
    private static final Logger logger = LoggerFactory.getLogger(PluginManager.class);
    
    private ConfigurableApplicationContext context;
    private Map<String, PluginWrapper> plugins;
    
    
    /**
     * Instanciate a {@link PluginManager}.
     * 
     * @param context Spring context (required)
     */
    public PluginManager(ConfigurableApplicationContext context)
    {
        Assert.notNull(context);
        
        this.context = context;
        this.plugins = new HashMap<>();
    }
    
    
    /**
     * Starting {@link PluginManager} to load and register plugins.
     */
    public void start()
    {
        if(!(context instanceof AnnotationConfigEmbeddedWebApplicationContext)) {
            logger.error("Context of type [{}] is not appropriate to register plugins", context.getClass().getName());
            return;
        }
        
        // Load and register plugins
        PluginLoader pluginLoader = new PluginLoader(context, this::registerPlugin);
        pluginLoader.loadPlugins();
        
        // Register Spring configurations of plugins
        // and create an extended ClassLoader
        if(!plugins.isEmpty())
        {   
            List<Class<?>> configurations = new ArrayList<>();
            PluginClassLoader extendedClassloader = new PluginClassLoader(context.getClassLoader());
            
            plugins.values().stream().forEach(pw -> {
                Collections.addAll(configurations, pw.getPlugin().configuration());
                extendedClassloader.addPluginClassLoader(pw.getClassLoader());
            });
            
            
            AnnotationConfigEmbeddedWebApplicationContext ctx = (AnnotationConfigEmbeddedWebApplicationContext)context;
            ctx.register(configurations.toArray(new Class<?>[configurations.size()]));
            
            ((DefaultResourceLoader)context).setClassLoader(extendedClassloader);
        }
    }
    
    /**
     * Checks and register the given plugin.
     * 
     * @param pluginURL Plugin URL (required)
     * @param pluginClassLoader Plugin {@link ClassLoader} (required)
     */
    private void registerPlugin(URL pluginURL, ClassLoader pluginClassLoader)
    {
        Assert.notNull(pluginURL);
        Assert.notNull(pluginClassLoader);
        
        
        // Search Plugin class (she must be unique by plugin)
        ServiceLoader<Plugin> pluginLoader = ServiceLoader.load(Plugin.class, pluginClassLoader);
        
        List<Plugin> pluginInstances = new ArrayList<>();
        pluginLoader.forEach(plugin -> pluginInstances.add(plugin));
        
        if(pluginInstances.isEmpty()) {
            logger.warn("No plugin class found in [{}], plugin is rejected", pluginURL.toString());
            return;
        }
        
        if(pluginInstances.size() > 1) {
            logger.warn("More than one plugin found in [{}], plugin is rejected", pluginURL.toString());
            return;
        }
        
        // Check Plugin class
        Plugin plugin = pluginInstances.get(0);
        
        PluginWrapper alreadyPresent = plugins.get(plugin.id());
        if(alreadyPresent != null) {
            logger.warn("Plugin [{}] is already present in version [{}], plugin [{}] is rejected", plugin.id(), alreadyPresent.getPlugin().version(), pluginURL.toString());
            return;
        }
        
        if(ArrayUtils.isEmpty(plugin.configuration())) {
            logger.warn("No configuration class found in [{}], plugin is rejected", pluginURL.toString());
            return;
        }
        
        // Register the plugin
        plugins.put(plugin.id(), new PluginWrapper(plugin, pluginClassLoader));
    }

    
    /**
     * Get all plugins.
     * 
     * @return Plugins list (may be empty, but not null)
     */
    public List<Plugin> getPlugins()
    {
        if(plugins.isEmpty()) {
            return new ArrayList<>();
        }
        
        return plugins.values().stream()
                      .map(pw -> { return pw.getPlugin(); })
                      .collect(Collectors.toList());
    }
}
