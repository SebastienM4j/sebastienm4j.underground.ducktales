package xyz.smouquet.ducktales.app.module.banking.logging;

import xyz.smouquet.ducktales.app.module.banking.data.entity.Account;

/**
 * Banking module MDC (http://logback.qos.ch/manual/mdc.html) constants.
 * 
 * @author smouquet
 */
public final class BankingMDC
{
    /** MDC for {@link Account#getId()} */
    public static final String ACT = "act.id";

}
