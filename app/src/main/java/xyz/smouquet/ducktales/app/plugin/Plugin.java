package xyz.smouquet.ducktales.app.plugin;

/**
 * This is the a plugin entry point. It discribes
 * the plugin and list the configurations classes.
 * 
 * @author smouquet
 */
public interface Plugin
{
    /**
     * @return Plugin's identifier, that need to be unique
     */
    String id();
    
    /**
     * @return Version number of the plugin
     */
    String version();
    
    /**
     * @return Plugin's name
     */
    String name();
    
    /**
     * @return Description of the plugin
     */
    String description();
    
    /**
     * @return Spring configuration classes
     *         to register in the application context
     */
    Class<?>[] configuration();
}
