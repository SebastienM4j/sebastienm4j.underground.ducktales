package xyz.smouquet.ducktales.app.module.base.web.dto;

import org.springframework.util.Assert;

import xyz.smouquet.ducktales.app.plugin.Plugin;

/**
 * DTO for {@link Plugin} informations.
 * 
 * @author smouquet
 */
public class PluginDTO
{
    private String id;
    private String version;
    private String name;
    private String description;
   
    
    /**
     * Instanciate the DTO with a {@link Plugin}.
     * 
     * TODO Utiliser un outil de mapping automatique (ex : orika)
     * 
     * @param plugin Source {@link Plugin} (required)
     */
    public PluginDTO(Plugin plugin)
    {
        Assert.notNull(plugin);
        
        this.id = plugin.id();
        this.version = plugin.version();
        this.name = plugin.name();
        this.description = plugin.description();
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
