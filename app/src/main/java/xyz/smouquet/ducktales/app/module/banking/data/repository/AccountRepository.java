package xyz.smouquet.ducktales.app.module.banking.data.repository;

import java.util.List;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import xyz.smouquet.ducktales.app.module.banking.data.entity.Account;

/**
 * Account data repository.
 * 
 * @author smouquet
 */
public interface AccountRepository extends ElasticsearchRepository<Account, String>
{
    /**
     * Find all accounts of a wallet.
     * 
     * @param walletId Id of wallet for which get accouts list
     * @return List of accounts
     */
    List<Account> findByWalletIdOrderByOrderAsc(String walletId);
}
