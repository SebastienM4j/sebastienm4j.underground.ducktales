package xyz.smouquet.ducktales.app.plugin;

import org.springframework.util.Assert;

/**
 * Conserves a unique instance of {@link PluginManager}.
 * 
 * @author sebastien
 */
public class PluginManagerHolder
{
    private static PluginManager PLUGIN_MANAGER;
    
    /**
     * @return The {@link PluginManager}
     */
    public static PluginManager get()
    {
        return PLUGIN_MANAGER;
    }
    
    /**
     * Set the {@link PluginManager} if is not already set.
     * 
     * @param pluginManager {@link PluginManager} to set
     */
    public static void set(PluginManager pluginManager)
    {
        Assert.notNull(pluginManager);
        
        if(PLUGIN_MANAGER == null) {
            PLUGIN_MANAGER = pluginManager;
        }
    }
}
