package xyz.smouquet.ducktales.app.module.banking.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * Banking module configuration.
 * 
 * @author smouquet
 */
@Configuration
@EnableElasticsearchRepositories({
    "xyz.smouquet.ducktales.app.module.banking.data.repository"
})
@ComponentScan({
    "xyz.smouquet.ducktales.app.module.banking.service",
    "xyz.smouquet.ducktales.app.module.banking.web.resource"
})
public class ModuleBankingSpringConfig
{

    /**
     * @return Elasticsearch index name
     */
    @Bean
    @Qualifier("indexName")
    public String bankingIndexName() {
        return "ducktales-banking";
    }
    
}
