package xyz.smouquet.ducktales.app.module.base.logging;

import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/**
 * Markers used to define a group of destination of the logs.
 * 
 * @author smouquet
 */
public final class Markers
{
    public static final Marker AUDIT = MarkerFactory.getMarker("AUDIT");
}
