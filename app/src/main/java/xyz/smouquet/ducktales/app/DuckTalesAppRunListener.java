package xyz.smouquet.ducktales.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringApplicationRunListener;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;

import xyz.smouquet.ducktales.app.plugin.PluginManager;
import xyz.smouquet.ducktales.app.plugin.PluginManagerHolder;

/**
 * Listen for the bootstrap of Spring boot application.
 * 
 * @author smouquet
 */
public class DuckTalesAppRunListener implements SpringApplicationRunListener
{
    public DuckTalesAppRunListener(SpringApplication app, String[] args) {}
    

    @Override
    public void environmentPrepared(ConfigurableEnvironment environment) {}

    @Override
    public void contextPrepared(ConfigurableApplicationContext context)
    {
        PluginManager pluginManager = new PluginManager(context);
        pluginManager.start();
        
        PluginManagerHolder.set(pluginManager);
    }

    @Override
    public void contextLoaded(ConfigurableApplicationContext context) {}

    @Override
    public void started() {}
    
    @Override
    public void finished(ConfigurableApplicationContext context, Throwable exception) {}

}
