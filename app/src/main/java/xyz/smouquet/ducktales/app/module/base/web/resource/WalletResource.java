package xyz.smouquet.ducktales.app.module.base.web.resource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import xyz.smouquet.ducktales.app.module.base.data.entity.Wallet;
import xyz.smouquet.ducktales.app.module.base.data.repository.WalletRepository;
import xyz.smouquet.ducktales.app.module.base.exception.EntityNotFoundException;
import xyz.smouquet.ducktales.app.module.base.service.WalletService;

/**
 * Wallet REST resource for GUI uses.
 * 
 * @author smouquet
 */
@RestController
@RequestMapping("/rest/gui/wallets")
public class WalletResource
{
    @Autowired
    private WalletRepository walletRepository;
    
    @Autowired
    private WalletService walletService;
    
    
    /**
     * @return All wallets in ascending order
     */
    @RequestMapping(method=RequestMethod.GET)
    public List<Wallet> getAll()
    {
        List<Wallet> wallets = new ArrayList<>();
        
        Iterator<Wallet> iterator = walletRepository.findAll(new Sort("order")).iterator();
        while(iterator.hasNext()) {
            wallets.add(iterator.next());
        }
        
        return wallets;
    }
    
    /**
     * Create a new wallet.
     * 
     * @param wallet Wallet to create
     * @return The created wallet
     * @see WalletService#create(Wallet)
     */
    @RequestMapping(method=RequestMethod.POST)
    public Wallet create(@RequestBody Wallet wallet)
    {
        return walletService.create(wallet);
    }
    
    /**
     * Update a wallet.
     * 
     * @param walletId Id of wallet to update
     * @param wallet Wallet to update
     * @see WalletService#update(Wallet)
     */
    @RequestMapping(value="/{walletId}", method=RequestMethod.PUT)
    public void update(@PathVariable String walletId, @RequestBody Wallet wallet)
    {
        Assert.notNull(wallet);
        Assert.isTrue(walletId.equals(wallet.getId()));
            
        walletService.update(wallet);
    }
    
    /**
     * Delete a wallet.
     * 
     * @param walletId Id of wallet to delete
     * @see WalletService#delete(Wallet)
     */
    @RequestMapping(value="/{walletId}", method=RequestMethod.DELETE)
    public void delete(@PathVariable String walletId)
    {
        Wallet wallet = walletRepository.findOne(walletId);
        if(wallet == null) {
            throw new EntityNotFoundException();
        }
        
        walletService.delete(wallet);
    }
    
}
