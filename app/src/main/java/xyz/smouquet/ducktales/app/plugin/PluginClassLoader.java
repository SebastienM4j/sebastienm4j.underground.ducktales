package xyz.smouquet.ducktales.app.plugin;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import org.springframework.util.Assert;

/**
 * {@link ClassLoader} that sees all plugins. 
 * It looks for classes or resources in all plugins.
 * 
 * @author smouquet
 */
public class PluginClassLoader extends ClassLoader
{
    private List<ClassLoader> pluginsClassLoader;
    
    
    /**
     * New instance with a parent {@link ClassLoader}.
     * 
     * @param parent Parent {@link ClassLoader}
     */
    public PluginClassLoader(ClassLoader parent)
    {
        super(parent);
        this.pluginsClassLoader = new ArrayList<>();
    }
    
    
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException
    {
        for(ClassLoader cl : pluginsClassLoader)
        {
            try {
                return cl.loadClass(name);
            
            } catch(ClassNotFoundException ex) {
                // try next
            }
        }
        
        throw new ClassNotFoundException(name);
    }
    
    @Override
    protected URL findResource(String name)
    {
        for(ClassLoader cl : pluginsClassLoader)
        {
            URL url = cl.getResource(name);
            if(url != null) {
                return url;
            }
        }
        
        return null;
    }
    
    @Override
    protected Enumeration<URL> findResources(String name) throws IOException
    {
        List<URL> resources = new ArrayList<URL>();
        for(ClassLoader cl : pluginsClassLoader) {
            resources.addAll(Collections.list(cl.getResources(name)));
        }

        return Collections.enumeration(resources);
    }
    
    
    /**
     * Add's a plugin {@link ClassLoader} to this {@link ClassLoader}.
     * 
     * @param pluginClassLoader The {@link ClassLoader} of a plugin (required)
     */
    public void addPluginClassLoader(ClassLoader pluginClassLoader)
    {
        Assert.notNull(pluginClassLoader);
        pluginsClassLoader.add(pluginClassLoader);
    }
    
}
