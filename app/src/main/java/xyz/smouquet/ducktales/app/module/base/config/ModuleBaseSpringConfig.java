package xyz.smouquet.ducktales.app.module.base.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

/**
 * Base module configuration.
 * 
 * @author smouquet
 */
@Configuration
@EnableElasticsearchRepositories({
    "xyz.smouquet.ducktales.app.module.base.data.repository"
})
@ComponentScan({
    "xyz.smouquet.ducktales.app.module.base.service",
    "xyz.smouquet.ducktales.app.module.base.web.resource"
})
public class ModuleBaseSpringConfig
{

    /**
     * @return Elasticsearch index name
     */
    @Bean
    @Qualifier("indexName")
    public String baseIndexName() {
        return "ducktales-base";
    }
    
}
