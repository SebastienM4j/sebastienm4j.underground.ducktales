package xyz.smouquet.ducktales.app.plugin;

/**
 * Wrap a plugin and its associated.
 * 
 * @author smouquet
 */
public class PluginWrapper
{
    private Plugin plugin;
    private ClassLoader classLoader;
    
    
    public PluginWrapper() {}
    
    public PluginWrapper(Plugin plugin, ClassLoader classLoader)
    {
        this.plugin = plugin;
        this.classLoader = classLoader;
    }
    
    
    public Plugin getPlugin() {
        return plugin;
    }
    
    public void setPlugin(Plugin plugin) {
        this.plugin = plugin;
    }
    
    public ClassLoader getClassLoader() {
        return classLoader;
    }
    
    public void setClassLoader(ClassLoader classLoader) {
        this.classLoader = classLoader;
    }
}
