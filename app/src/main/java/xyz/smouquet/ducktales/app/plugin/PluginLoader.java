package xyz.smouquet.ducktales.app.plugin;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.function.BiConsumer;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.util.Assert;

/**
 * It's responsible of loading plugins. A {@link ClassLoader} is created
 * for each plugin to load classes and resources of this one.
 * 
 * @author smouquet
 */
public class PluginLoader
{
    private static final Logger logger = LoggerFactory.getLogger(PluginLoader.class);
    
    private static final String PLUGINS_DIR_PROPERTY = "ducktales.plugins.dir";
    
    private ConfigurableApplicationContext context;
    private BiConsumer<URL, ClassLoader> pluginRegister;
    
    
    /**
     * Instanciate a {@link PluginLoader}.
     * 
     * @param context Spring context to get environment properties and {@link ClassLoader} (required)
     * @param pluginRegister Function responsible to register/initialize one plugin (required)
     */
    public PluginLoader(ConfigurableApplicationContext context, BiConsumer<URL, ClassLoader> pluginRegister)
    {
        Assert.notNull(context);
        Assert.notNull(pluginRegister);
        
        this.context = context;
        this.pluginRegister = pluginRegister;
    }
    
    
    /**
     * Load plugins.
     */
    public void loadPlugins()
    {
        // Prepare the plugins directory File
        String pluginsDirPath = context.getEnvironment().getProperty(PLUGINS_DIR_PROPERTY);
        if(StringUtils.isBlank(pluginsDirPath)) {
            logger.info("No set of plugins directory (if needed, set '{}' variable)", PLUGINS_DIR_PROPERTY);
            return;
        }
        
        File pluginsDir = new File(pluginsDirPath);
        if(!pluginsDir.exists() || !pluginsDir.isDirectory()) {
            logger.warn("Plugins directory [{}] does not exists or isn't a directory");
            return;
        }
        
        logger.info("Plugins will be loaded from [{}]", pluginsDirPath);
        
        
        // Search jar in the directory
        File[] pluginsFile = pluginsDir.listFiles((dir, name) -> name.endsWith(".jar"));
        if(pluginsFile.length == 0) {
            logger.info("No plugin to load");
            return;
        }
        
        
        URL url = null;
        ClassLoader pluginClassLoader;
        for(File pluginFile : pluginsFile)
        {
            try
            {
                url = pluginFile.toURI().toURL();
                
                logger.info("Loading plugin [{}]", url.toString());
                pluginClassLoader = new URLClassLoader(new URL[]{url}, context.getClassLoader());
                
                pluginRegister.accept(url, pluginClassLoader);
                
            } catch(MalformedURLException ex) {
                logger.error("Loading plugin [{}] fail", url != null ? url.toString() : "");
            }
        }
    }
}
