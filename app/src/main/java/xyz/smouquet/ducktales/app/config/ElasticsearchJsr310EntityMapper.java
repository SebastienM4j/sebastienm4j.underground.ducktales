package xyz.smouquet.ducktales.app.config;

import java.io.IOException;

import org.springframework.data.elasticsearch.core.EntityMapper;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Elasticsearch {@link EntityMapper} for Java 8 Date/Time API support.
 * 
 * @author smouquet
 */
public class ElasticsearchJsr310EntityMapper implements EntityMapper
{
    private ObjectMapper objectMapper;
    
    
    public ElasticsearchJsr310EntityMapper(Jackson2ObjectMapperBuilder builder)
    {
        objectMapper = builder.createXmlMapper(false).build();
    }
    

    @Override
    public <T> T mapToObject(String source, Class<T> clazz) throws IOException
    {
        return objectMapper.readValue(source, clazz);
    }

    @Override
    public String mapToString(Object object) throws IOException
    {
        return objectMapper.writeValueAsString(object);
    }
    
}
