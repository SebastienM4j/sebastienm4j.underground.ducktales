/**
 * Main sidenav.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.component.main.sidenav', []);

(function() {
    'use strict';

    angular.module('xyz.smouquet.ducktales.component.main.sidenav')
           .directive('dktMainSidenav', mainSidevav);
  
    function mainSidevav()
    {
        return {
            restrict: 'E',
            templateUrl: 'app/components/main/sidenav.html',
            replace: true,
            scope: {},
            controller: MainSidenavCtrl,
            controllerAs: 'ctrl',
            bindToController: {
                collapsed: '='
            }
        };
    }

    function MainSidenavCtrl($scope, $mdMedia, $stateParams)
    {
        var $this = this;
        
        
        // When active wallet or account changed 
        
        $scope.$watch(function() { return $stateParams.walletId; }, function(walletId) {
            if(walletId) {
                $this.walletId = walletId;
            } else {
                $this.walletId = null;
            }
        });
        
        $scope.$watch(function() { return $stateParams.accountId; }, function(accountId) {
            if(accountId) {
                $this.accountId = accountId;
            } else {
                $this.accountId = null;
            }
        });
        
        
        // Menu size
        
        this.collapsed = false;
        this.onCollapseExpand = function() {
            this.collapsed = !this.collapsed;
        };
        
        $scope.$watch(function() { return $mdMedia('gt-lg'); }, function(large) {
            $this.collapsed = !large;
        });
    }
})();
