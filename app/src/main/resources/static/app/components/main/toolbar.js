/**
 * Main toolbar.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.component.main.toolbar', []);

(function() {
    'use strict';

    angular.module('xyz.smouquet.ducktales.component.main.toolbar')
           .directive('dktMainToolbar', mainToolbar);
  
    function mainToolbar()
    {
        return {
            restrict: 'E',
            templateUrl: 'app/components/main/toolbar.html',
            replace: true,
            scope: {},
            controller: MainToolbarCtrl,
            controllerAs: 'ctrl'
        };
    }

    function MainToolbarCtrl(Restangular, $state, $stateParams, $scope)
    {
        var $this = this;
        
        this.showWallets = false;
        this.showAccounts = false;
        
        
        // Methods to load wallets and accounts
        
        this.loadWallets = function()
        {
            Restangular.all('wallets').getList().then(function(wallets) {
                $this.wallets = wallets;
            });
        };
        
        this.loadAccounts = function()
        {
            Restangular.one('wallets', $this.selectedWalletId).all('accounts').getList().then(function(accounts) {
                $this.accounts = accounts;
            });
        };
        
        this.loadWallets();
        
        
        // When select wallet or account in lists
        
        this.onWalletChange = function()
        {
            if($stateParams.accountId) {
                $state.go('accounts-list', {walletId: $this.selectedWalletId});
            } else {
                $state.go($state.$current, {walletId: $this.selectedWalletId});
            }
            
            $this.loadAccounts();
        };
        
        this.onAccountChange = function()
        {
            $state.go($state.$current, {walletId: $this.selectedWalletId, accountId: $this.selectedAccountId});
        };
        
        
        // When active wallet or account changed 
        
        $scope.$watch(function() { return $stateParams.walletId; }, function(walletId) {
            if(walletId) {
                $this.selectedWalletId = walletId;
                $this.showWallets = true;
                
                $this.loadAccounts();
            } else {
                $this.showWallets = false;
                $this.selectedWalletId = null;
                $this.showAccounts = false;
                $this.selectedAccountId = null;
                $this.accounts = null;
            }
        });
        
        $scope.$watch(function() { return $stateParams.accountId; }, function(accountId) {
            if(accountId) {
                $this.selectedAccountId = accountId;
                $this.showAccounts = true;
            } else {
                $this.showAccounts = false;
                $this.selectedAccountId = null;
            }
        });
        
        
        // When on wallet or account is created, updated or deleted

        $scope.$on('walletEvent', function(event, walletEvent)
        {
            switch(walletEvent.event)
            {
                case 'created':
                    $this.wallets.push(walletEvent.wallet);
                    _.sortBy($this.wallets, 'order');
                    break;
                case 'updated':
                    var idx = _.findIndex($this.wallets, function(w) {
                        return w.id == walletEvent.wallet.id;
                    });
                    $this.wallets[idx] = walletEvent.wallet;
                    _.sortBy($this.wallets, 'order');
                    break;
                case 'deleted':
                    $this.wallets = _.reject($this.wallets, function(w) {
                        return w.id == walletEvent.wallet.id;
                    });
                    break;
            }
        });
        
        $scope.$on('accountEvent', function(event, accountEvent)
        {
            switch(accountEvent.event)
            {
                case 'created':
                    $this.accounts.push(accountEvent.account);
                    _.sortBy($this.accounts, 'order');
                    break;
                case 'updated':
                    var idx = _.findIndex($this.accounts, function(a) {
                        return a.id == accountEvent.account.id;
                    });
                    $this.accounts[idx] = accountEvent.account;
                    _.sortBy($this.accounts, 'order');
                    break;
                case 'deleted':
                    $this.accounts = _.reject($this.accounts, function(a) {
                        return a.id == accountEvent.account.id;
                    });
                    break;
            }
        });
    }
})();
