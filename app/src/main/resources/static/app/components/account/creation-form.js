/**
 * Account creation form.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.component.account.creation-form', []);

(function() {
    'use strict';

    angular.module('xyz.smouquet.ducktales.component.account.creation-form')
           .directive('dktAccountCreationForm', accountCreationForm);
  
    function accountCreationForm()
    {
        return {
            restrict: 'E',
            templateUrl: 'app/components/account/creation-form.html',
            replace: true,
            scope: {},
            controller: AccountCreationFormCtrl,
            controllerAs: 'ctrl',
            bindToController: {
                accountsCount: '=',
                afterCreate: '&'
            }
        };
    }

    function AccountCreationFormCtrl(Restangular, $stateParams)
    {
        var $this = this;
        
        this.newAccount= {};
        
        this.onCreate = function()
        {
            if(this.newAccount.name && this.newAccount.name.length > 0)
            {
                this.newAccount.order = this.accountsCount+1;
                
                Restangular.one('wallets', $stateParams.walletId).all('accounts').post(this.newAccount).then(function(createdAccount)
                {
                    $this.newAccount =  {};
                    $this.afterCreate({'createdAccount': createdAccount});
                });
            }
        };
    }
})();
