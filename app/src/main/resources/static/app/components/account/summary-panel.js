/**
 * Panel for a account summary.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.component.account.summary-panel', []);

(function() {
    'use strict';

    angular.module('xyz.smouquet.ducktales.component.account.summary-panel')
           .directive('dktAccountSummaryPanel', accountSummaryPanel);
  
    function accountSummaryPanel()
    {
        return {
            restrict: 'E',
            templateUrl: 'app/components/account/summary-panel.html',
            replace: true,
            scope: {},
            controller: AccountSummaryPanelCtrl,
            controllerAs: 'ctrl',
            bindToController: {
                walletId: '=',
                account: '=',
                afterEditCallback: '&afterEdit',
                afterDeleteCallback: '&afterDelete'
            }
        };
    }

    function AccountSummaryPanelCtrl(Restangular, $stateParams, $mdDialog)
    {
        var $this = this;
        
        this.editing = false;
        this.onEdit = function() {
            this.editing = true;
        };
        this.onEditCancel = function() {
            this.editing = false;
        };
        
        this.onEditSave = function()
        {
            if(this.account.name && this.account.name.length > 0)
            {
                this.account.put().then(function() {
                    $this.editing = false;
                    if($this.afterEditCallback) {
                        $this.afterEditCallback({'account': $this.account});
                    }
                });
            }
        };
        
        this.onDelete = function(event)
        {
            var confirm = $mdDialog.confirm()
                                   .title('Confirmer la suppression du compte ?')
                                   .content('Le compte, ainsi que toutes ces opérations seront supprimés.')
                                   .targetEvent(event)
                                   .ok('Supprimer')
                                   .cancel('Conserver le compte');
            
            $mdDialog.show(confirm).then(function()
            {
                Restangular.one('wallets', $stateParams.walletId).one('accounts', $this.account.id).remove().then(function() {
                    if($this.afterDeleteCallback) {
                        $this.afterDeleteCallback({'account': $this.account});
                    }
                });
            });
        };
    }
})();
