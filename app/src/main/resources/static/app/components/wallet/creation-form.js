/**
 * Wallet creation form.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.component.wallet.creation-form', []);

(function() {
    'use strict';

    angular.module('xyz.smouquet.ducktales.component.wallet.creation-form')
           .directive('dktWalletCreationForm', walletCreationForm);
  
    function walletCreationForm()
    {
        return {
            restrict: 'E',
            templateUrl: 'app/components/wallet/creation-form.html',
            replace: true,
            scope: {},
            controller: WalletCreationFormCtrl,
            controllerAs: 'ctrl',
            bindToController: {
                walletsCount: '=',
                afterCreate: '&'
            }
        };
    }

    function WalletCreationFormCtrl(Restangular)
    {
        var $this = this;
        
        this.newWallet = {};
        
        this.onCreate = function()
        {
            if(this.newWallet.name && this.newWallet.name.length > 0)
            {
                this.newWallet.order = this.walletsCount+1;
                
                Restangular.all('wallets').post(this.newWallet).then(function(createdWallet)
                {
                    $this.newWallet = {};
                    $this.afterCreate({'createdWallet': createdWallet});
                });
            }
        };
    }
})();
