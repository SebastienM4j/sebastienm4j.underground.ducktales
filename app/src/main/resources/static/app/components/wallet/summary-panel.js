/**
 * Panel for a wallet summary.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.component.wallet.summary-panel', []);

(function() {
    'use strict';

    angular.module('xyz.smouquet.ducktales.component.wallet.summary-panel')
           .directive('dktWalletSummaryPanel', walletSummaryPanel);
  
    function walletSummaryPanel()
    {
        return {
            restrict: 'E',
            templateUrl: 'app/components/wallet/summary-panel.html',
            replace: true,
            scope: {},
            controller: WalletSummaryPanelCtrl,
            controllerAs: 'ctrl',
            bindToController: {
                wallet: '=',
                afterEditCallback: '&afterEdit',
                afterDeleteCallback: '&afterDelete'
            }
        };
    }

    function WalletSummaryPanelCtrl(Restangular, $mdDialog)
    {
        var $this = this;
        
        this.editing = false;
        this.onEdit = function() {
            this.editing = true;
        };
        this.onEditCancel = function() {
            this.editing = false;
        };
        
        this.onEditSave = function()
        {
            if(this.wallet.name && this.wallet.name.length > 0)
            {
                this.wallet.put().then(function() {
                    $this.editing = false;
                    if($this.afterEditCallback) {
                        $this.afterEditCallback({'wallet': $this.wallet});
                    }
                });
            }
        };
        
        this.onDelete = function(event)
        {
            var confirm = $mdDialog.confirm()
                                   .title('Confirmer la suppression du portefeuille ?')
                                   .content('Le portefeuille, ainsi que tous ces comptes seront supprimés.')
                                   .targetEvent(event)
                                   .ok('Supprimer')
                                   .cancel('Conserver le portefeuille');
            
            $mdDialog.show(confirm).then(function()
            {
                Restangular.one('wallets', $this.wallet.id).remove().then(function() {
                    if($this.afterDeleteCallback) {
                        $this.afterDeleteCallback({'wallet': $this.wallet});
                    }
                });
            });
        };
    }
})();
