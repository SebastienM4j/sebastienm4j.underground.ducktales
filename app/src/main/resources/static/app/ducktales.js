/**
 * Configuration and bootstrap of application.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.app', [
    'ngMaterial',
    'ui.router',
    'restangular',
    'ncy-angular-breadcrumb',
    'angularMoment',
    'oc.lazyLoad',
    
    'xyz.smouquet.ducktales.view.main.layout',
    'xyz.smouquet.ducktales.view.wallet.list',
    'xyz.smouquet.ducktales.view.account.list',
    'xyz.smouquet.ducktales.view.operation.list'
])

.config(['$mdThemingProvider', '$breadcrumbProvider', '$urlRouterProvider', 'RestangularProvider', 
 function($mdThemingProvider, $breadcrumbProvider, $urlRouterProvider, RestangularProvider)
{
    $mdThemingProvider.theme('default')
                       .primaryPalette('blue', {
                           'default': '500',
                           'hue-1': '100',
                           'hue-2': '600',
                           'hue-3': '900'
                        })
                       .accentPalette('blue-grey', {
                           'default': '500',
                           'hue-1': '300',
                           'hue-2': '600',
                           'hue-3': '900'
                        });
    
    $breadcrumbProvider.setOptions({
        templateUrl: 'app/components/main/breadcrumb.html'
    });
    
    $urlRouterProvider.otherwise('/');
    
    RestangularProvider.setBaseUrl('./rest/gui');
}])

.run(['amMoment', 'Restangular', '$ocLazyLoad', '$log', '$state', 
function (amMoment, Restangular, $ocLazyLoad, $log, $state)
{
	amMoment.changeLocale('fr');
	
	Restangular.all('plugins').getList().then(function(plugins)
    {
	    if(plugins && plugins.length > 0)
	    {
	        plugins.forEach(function(plugin, idx, array)
            {
	            var path = plugin.id+'/'+plugin.id+'.js';
	            $ocLazyLoad.load(path);
	        });
        }
    });
	
	$state.go('wallets-list');
}])
;
