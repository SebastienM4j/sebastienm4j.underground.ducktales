/**
 * Operations list.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.view.operation.list', [])

.config(['$stateProvider', function($stateProvider)
{
    $stateProvider.state('operations-list',
    {
        url: '/wallets/{walletId}/accounts/{accountId}/operations',
        ncyBreadcrumb: {
            label: 'Opérations'
        },
        templateUrl: 'app/views/operation/list.html',
        controller: 'OperationsListCtrl',
        controllerAs: 'ctrl'
    });
}])

.controller('OperationsListCtrl', ['$stateParams', 
                           function($stateParams)
{
    var $this = this;
    
    this.accountId = $stateParams.accountId;
}])
;