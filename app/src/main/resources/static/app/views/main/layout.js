/**
 * Main layout.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.view.main.layout', [
    'xyz.smouquet.ducktales.component.main.sidenav',
    'xyz.smouquet.ducktales.component.main.toolbar',
])

.directive('dktMainLayout', function()
{
    return {
        restrict: 'A',
        templateUrl: 'app/views/main/layout.html'
    };
})
;
