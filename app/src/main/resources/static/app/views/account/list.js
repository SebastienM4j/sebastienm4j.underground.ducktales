/**
 * Accounts list.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.view.account.list', [
    'xyz.smouquet.ducktales.component.account.summary-panel',
    'xyz.smouquet.ducktales.component.account.creation-form'
])

.config(['$stateProvider', function($stateProvider)
{
    $stateProvider.state('accounts-list',
    {
        url: '/wallets/{walletId}/accounts',
        ncyBreadcrumb: {
            label: 'Comptes'
        },
        templateUrl: 'app/views/account/list.html',
        controller: 'AccountsListCtrl',
        controllerAs: 'ctrl',
        resolve: {
            accounts: ['Restangular', '$stateParams', function(Restangular, $stateParams) {
                return Restangular.one('wallets', $stateParams.walletId).all('accounts').getList();
            }]
        }
    });
}])

.controller('AccountsListCtrl', ['accounts', '$stateParams', '$rootScope', 
                         function(accounts, $stateParams, $rootScope)
{
    var $this = this;
    
    this.walletId = $stateParams.walletId;
    this.accounts = accounts;
    
    
    this.onAfterCreate = function(createdAccount)
    {
        this.accounts.push(createdAccount);
        $rootScope.$broadcast('accountEvent', {event:'created',account:createdAccount});
    };
    
    this.onAfterEdit = function(account)
    {
        $rootScope.$broadcast('accountEvent', {event:'updated',account:account});
    };
    
    this.onAfterDelete = function(account)
    {
        this.accounts = _.reject(this.accounts, function(a) {
            return a.id == account.id;
        });
        $rootScope.$broadcast('accountEvent', {event:'deleted',account:account});
    };
}])
;