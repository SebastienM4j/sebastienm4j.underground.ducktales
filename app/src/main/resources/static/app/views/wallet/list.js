/**
 * Wallet list view.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.view.wallet.list', [
    'xyz.smouquet.ducktales.component.wallet.summary-panel',
    'xyz.smouquet.ducktales.component.wallet.creation-form'
])

.config(['$stateProvider', function($stateProvider)
{
    $stateProvider.state('wallets-list',
    {
        url: '/wallets',
        ncyBreadcrumb: {
            label: 'Portefeuilles'
        },
        templateUrl: 'app/views/wallet/list.html',
        controller: 'WalletsListCtrl',
        controllerAs: 'ctrl',
        resolve: {
            wallets: ['Restangular', function(Restangular) {
                return Restangular.all('wallets').getList();
            }]
        }
    });
}])

.controller('WalletsListCtrl', ['wallets', '$rootScope', 
                        function(wallets, $rootScope)
{
    this.wallets = wallets;
    
    this.onAfterCreate = function(createdWallet)
    {
        this.wallets.push(createdWallet);
        $rootScope.$broadcast('walletEvent', {event:'created',wallet:createdWallet});
    };
    
    this.onAfterEdit = function(wallet)
    {
        $rootScope.$broadcast('walletEvent', {event:'updated',wallet:wallet});
    };
    
    this.onAfterDelete = function(wallet)
    {
        this.wallets = _.reject(this.wallets, function(w) {
            return w.id == wallet.id;
        });
        $rootScope.$broadcast('walletEvent', {event:'deleted',wallet:wallet});
    };
}])
;