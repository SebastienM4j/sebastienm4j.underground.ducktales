package xyz.smouquet.ducktales.pluginhw2.config;

import xyz.smouquet.ducktales.app.plugin.Plugin;

public class PluginHw2 implements Plugin
{

    @Override
    public String id() {
        return "xyz.smouquet.ducktales.pluginhw2";
    }

    @Override
    public String version() {
        return "0.1";
    }

    @Override
    public String name() {
        return "plugin-hw2";
    }

    @Override
    public String description() {
        return "Plugin Hello World 2";
    }

    @Override
    public Class<?>[] configuration() {
        return new Class<?>[]{ PluginHw2SpringConfig.class };
    }

}
