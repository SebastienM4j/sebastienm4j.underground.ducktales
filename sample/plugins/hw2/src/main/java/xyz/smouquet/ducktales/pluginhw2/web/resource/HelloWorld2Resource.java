package xyz.smouquet.ducktales.pluginhw2.web.resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/gui/pluginhw2")
public class HelloWorld2Resource
{

    @RequestMapping(method=RequestMethod.GET)
    public String helloWorld()
    {
        return "Hello world from Rest 2 resource !";
    }
    
}
