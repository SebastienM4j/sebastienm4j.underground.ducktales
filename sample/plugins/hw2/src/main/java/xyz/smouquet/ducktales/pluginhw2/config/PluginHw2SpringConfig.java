package xyz.smouquet.ducktales.pluginhw2.config;

import org.apache.commons.codec.binary.Base64;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("xyz.smouquet.ducktales.pluginhw2.web.resource")
public class PluginHw2SpringConfig
{
    
    @Bean
    public String helloWorld2() {
        return "Hello Spring world 2 ! "+Base64.encodeBase64String("ducktales".getBytes());
    }
    
}
