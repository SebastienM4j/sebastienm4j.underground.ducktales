/**
 * Plugin HW 2.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.pluginhw2', [])

.run(['$log', function($log)
{
    $log.log("Hello world from Hw 2 plugin !");
}])
;
