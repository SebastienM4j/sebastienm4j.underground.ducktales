package xyz.smouquet.ducktales.pluginhw.config;

import org.apache.commons.codec.binary.Base64;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("xyz.smouquet.ducktales.pluginhw.web.resource")
public class PluginHwSpringConfig
{
    
    @Bean
    public String helloWorld1() {
        return "Hello Spring world 1 ! "+new String(Base64.encodeBase64("ducktales".getBytes()));
    }
    
}
