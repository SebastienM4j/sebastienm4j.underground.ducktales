package xyz.smouquet.ducktales.pluginhw.config;

import xyz.smouquet.ducktales.app.plugin.Plugin;

public class PluginHw implements Plugin
{

    @Override
    public String id() {
        return "xyz.smouquet.ducktales.pluginhw";
    }

    @Override
    public String version() {
        return "0.1";
    }

    @Override
    public String name() {
        return "plugin-hw";
    }

    @Override
    public String description() {
        return "Plugin Hello World";
    }

    @Override
    public Class<?>[] configuration() {
        return new Class<?>[]{ PluginHwSpringConfig.class };
    }

}
