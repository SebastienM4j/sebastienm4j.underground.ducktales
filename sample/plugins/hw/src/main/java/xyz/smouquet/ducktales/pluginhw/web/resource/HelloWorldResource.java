package xyz.smouquet.ducktales.pluginhw.web.resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/gui/pluginhw")
public class HelloWorldResource
{

    @RequestMapping(method=RequestMethod.GET)
    public String helloWorld()
    {
        return "Hello world from Rest resource !";
    }
    
}
