/**
 * Plugin HW.
 */
'use strict';

angular.module('xyz.smouquet.ducktales.pluginhw', [])

.run(['$log', function($log)
{
    $log.log("Hello world from Hw plugin !");
}])
;
